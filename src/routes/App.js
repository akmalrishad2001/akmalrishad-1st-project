import React from "react";
import { Routes, Route } from "react-router-dom";
import { Homepage } from "../pages/Homepage";
import { Navbar } from "../assets/components/Navbar";
import { BelajarSlice } from "../pages/BelajarSlice";
import { Tugas1 } from "../pages/Tugas1.jsx";
import { Login } from "../pages/Login";
import { Galeri } from "../pages/Galeri";
import { Pokemon } from "../pages/Pokemon";
import { PokemonDetail } from "../pages/PokemonDetail";

export const App = () => {
  return (
    <div>
      <Navbar></Navbar>
      <Routes>
        <Route path="/" element={<Pokemon />} />
        <Route path="/pokemon-detail" element={<PokemonDetail />} />
        <Route path="/login" element={<Login />} />
        <Route path="/homepage" element={<Homepage />} />
        <Route path="/about" element={<BelajarSlice />} />
        <Route path="/galeri" element={<Galeri />} />
      </Routes>
    </div>
  );
};
