import React, { useEffect } from 'react'
import "../css/global.css"

export const Card = (props) => { 
  
  return (
    <div className='card' onClick={props?.fungsi}>{props?.value}</div>
  )
}
