import React from 'react'
import { useNavigate } from 'react-router-dom'
import Kucing from '../img/kucing.jpg'

export const Navbar = () => {
    const navigate = useNavigate()
  return (
    <div>
        {/* <button onClick={()=>navigate('/homepage')}>homepage</button>
        <img alt="gambar kucing" src={Kucing} onClick={()=>navigate('/about')}></img>
        <input onCopy={()=>navigate('/galeri')}></input> */}
        <a href='/homepage'>homepage</a>
        <br/>
        <a href='/about'>about us</a>
        <br/>
        <a href='/galeri'>galeri</a>
        <br/>
        <a href='/'>login</a>
    </div>
  )
}
