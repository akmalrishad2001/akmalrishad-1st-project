import React from "react";
import Button from "@mui/material/Button";

export const BasicButton = (props) => {
  return (
    <Button variant="contained" onClick={props?.fungsi}>
      {props?.title}
    </Button>
  );
};
