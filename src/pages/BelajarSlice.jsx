import React from 'react'
import '../assets/css/belajarSlice.css'

export const BelajarSlice = () => {
  return (
    <div className='container-box'>
        <div className='header-box'>
            header
        </div>
        <div className='main-box'>
            <div className='main-left-box'>kiri</div>
            <div className='main-right-box'>
                <div className='main-right-up-box'>
                    <div className='main-card'>kanan atas 1</div>
                    <div className='main-card'>kanan atas 2</div>
                    <div className='main-card'>kanan atas 3</div>
                    <div className='main-card'>kanan atas 4</div>
                </div>
                <div className='main-right-down-box'>kanan bawah</div>
            </div>
        </div>
    </div>
  )
}
