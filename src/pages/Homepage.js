import React, { useState, useEffect } from "react";
import "../assets/css/Homepage.css";
import Kucing from "../assets/img/kucing.jpg";
import { BasicButton } from "../assets/components/Button";
import { Card } from "../assets/components/Card";
import "../assets/css/global.css";

//rafc => shortcut untuk membuat arrow function
export const Homepage = () => {
  const [Nama, setNama] = useState("");
  const [Nama2, setNama2] = useState("");
  const [Alamat, setAlamat] = useState("");
  const [Umur, setUmur] = useState("25");

  const nameHandler = () => {
    setNama("Roronoa King of Hell");
  };

  useEffect(() => {
    setNama("Akmal Rishad");
  }, []);

  useEffect(() => {
    if (Umur == 18) {
      setNama2("Akmal Rishad");
    } else {
      setNama2("kamu");
    }
  }, [Umur]);

  return (
    <div>
      <h1 className="text-red">Nama: {Nama} </h1>
      <h2 className="text-big-blue">Umur: {Umur}</h2>
      <h2>Alamat: {Alamat}</h2>
      {/* <img src={Kucing} alt="kucing.img"></img> */}
      <input
        type="text"
        onChange={(e) => {
          setAlamat(e.target.value);
        }}
      ></input>
      <button
        onClick={() => {
          setNama("Roronoa king of hell");
        }}
      >
        ganti nama
      </button>
      <select
        onChange={(e) => {
          setUmur(e.target.value);
        }}
      >
        <option>18</option>
        <option>20</option>
        <option>21</option>
      </select>
      <BasicButton title="ganti nama2" fungsi={nameHandler}></BasicButton>
      {/* <Button
        title="ganti alamat"
        fungsi={() => {
          setAlamat("Bandung");
        }}
      ></Button> */}
      <Card value="aku"></Card>
      <Card
        value={Nama2}
        fungsi={() => {
          setNama2("karrakter");
        }}
      ></Card>
      <Card value={Nama}></Card>
      <Card value="kita"></Card>
    </div>
  );
};
