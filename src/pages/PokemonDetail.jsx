import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'

export const PokemonDetail = () => {
    const dataState = useLocation()
    // console.log(dataState)
    const [Params, setParams] = useState(dataState.state)
    const [PokemonDetail, setPokemonDetail] = useState(undefined)

    const getPokemonDetail = () =>{
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: `${process.env.REACT_APP_BASE_URL}/api/v2/pokemon/${Params}`,
            headers: { 
              'Content-Type': 'application/x-www-form-urlencoded'
            },
            // data : data
          };
          
          axios.request(config)
          .then((response) => {
            console.log(response.data);
            setPokemonDetail(response.data)
          })
          .catch((error) => {
            console.log(error);
          });
          
    }

    useEffect(() => {
      getPokemonDetail()
    }, [])

    const renderSprites = () =>{
        if(PokemonDetail){
            const dataSprites = PokemonDetail?.sprites ? PokemonDetail?.sprites : [];
            return <div>
                <img src={dataSprites?.front_default}></img>
            </div>
        }
    }
    
  return (

    <div>
      
        <div>{renderSprites()}</div>
    </div>
  )
}
