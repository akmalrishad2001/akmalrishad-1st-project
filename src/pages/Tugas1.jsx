import React from "react";
import SimpleAccordion from "../assets/components/SimpleAccordion";
import MenuBookIcon from "@mui/icons-material/MenuBook";
import LocalPhoneIcon from '@mui/icons-material/LocalPhone';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import HomeIcon from '@mui/icons-material/Home';
import "../assets/css/tugas1.css";
import { useState } from "react";

export const Tugas1 = () => {
    const [NamaBuku, setNamaBuku] = useState("Harry Pottter");
  const [NamaOrang, setNamaOrang] = useState("Jk.Rowling");
  const [Nomor, setNomor] = useState("08080808080");
  const [Alamat, setAlamat] = useState("bandung");

  const clearHandler = () =>{
    setAlamat("")
    setNamaBuku("")
    setNamaOrang("")
    setNomor("")
  }

  return (
    <div className="container-box">
      <div className="container-up">
        <div className="container-left">
          <div className="card-icon">
            <MenuBookIcon></MenuBookIcon>
          </div>
          <div className="card-icon"><LocalPhoneIcon></LocalPhoneIcon></div>
          <div className="card-icon"><AccountCircleIcon></AccountCircleIcon></div>
          <div className="card-icon"><HomeIcon></HomeIcon></div>
        </div>
        <div className="container-right">
          <div className="card-name"> {NamaBuku}</div>
          <div className="card-name">{NamaOrang}</div>
          <div className="card-name">{Nomor}</div>
          <div className="card-name">{Alamat}</div>
        </div>
        <div className="clear" onClick={clearHandler}>clear</div>
      </div>
      <div>
        <SimpleAccordion></SimpleAccordion>
      </div>
    </div>
  );
};
