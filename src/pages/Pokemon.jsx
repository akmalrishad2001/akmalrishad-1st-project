import React, { useState, useEffect } from 'react'
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

export const Pokemon = () => {
    const navigate = useNavigate()

    const [Pokemon, setPokemon] = useState([])

    const getPokemonSpecies = () =>{
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: `${process.env.REACT_APP_BASE_URL}/api/v2/pokemon/`,
            headers: { 
              'Content-Type': 'application/x-www-form-urlencoded'
            },
            // data : data
          };
          
          axios.request(config)
          .then((response) => {
            console.log(response.data.results);
            setPokemon(response.data.results)
          })
          .catch((error) => {
            console.log(error);
          });
          
    }

    useEffect(() => {
      getPokemonSpecies()
    }, [])

    // const detailHandler=(name)=>{
    //     navigate('/pokemon-detail', {state:name})
    // }
    
  return (
    <div>
      <h1>{process.env.REACT_APP_NAME}</h1>
        {Pokemon.map(a=>{
            return <div onClick={()=>navigate('/pokemon-detail',{state:a.name})}>
                {a.name}
            </div>
        })}
    </div>
  )
}
